FROM python:3.5

ENV PYTHONUNBUFFERED 1

RUN mkdir /src

RUN mkdir /config

COPY /config/requirements.txt /config/

RUN pip install -r /config/requirements.txt
