# Myproject

Proyecto de ejemplo con Docker, Django, Nginx y Postgres

### Prerequisitos

Instalar docker y pip en ubuntu/debian

```
sudo apt-get install docker pip
```

### Installing

Construir docker

```
docker-compose build
```

Ejecutar docker

```
docker-compose up -d
```

Para detener la ejecucion

```
docker-compose stop
```

Para volver a iniciar las instancias

```
docker-compose start
```

Para acceso a la shell

```
# Nginx
docker-compose exec nginx bash

# Web
docker-compose exec web bash

# DB
docker-compose exec db bash
```

Para ver logs

```
# Nginx
docker-compose logs nginx  

# Web
docker-compose logs web  

# DB
docker-compose logs db  
```
